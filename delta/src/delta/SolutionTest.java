package delta;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void testSolutionWithDefaultArray() {
        int[] a = { 1, 5, 2, -2 };
        assertEquals(0, new Solution().solution(a));
    }

    @Test
    public void testSolutionWithDuplicateValue() {
        int[] a = { 3, -9, -1, -2, 1 };
        assertEquals(2, new Solution().solution(a));
    }

    @Test
    public void testSolutionWithTrickyArray() {
        int[] a = { -72, -78, 22, -61, 77 };
        assertEquals(0, new Solution().solution(a));        
    }
    
    @Test
    public void testSolutionWithRandomArray() {
        Random random = new Random();
        int[] a = new int[5];
        for (int i = 0; i < a.length; i++) {
            a[i] = random.nextInt(21) - 10;
        }
        int expected = solveExhaustiveSearch(a);
        int actual = new Solution().solution(a);
        assertEquals(arrayToString(a), expected, actual);
    }

    private String arrayToString(int[] a) {
        StringBuilder sb = new StringBuilder("[ ");
        for (int i = 0; i < a.length; i++) {
            sb.append(a[i]);
            sb.append(' ');
        }
        sb.append("]");
        return sb.toString();
    }
    
    private int solveExhaustiveSearch(int[] a) {
        int res = Integer.MAX_VALUE;
        for (int i = (int) Math.pow(2, a.length) - 1; i >= 0; i--) {
            int sum = 0;
            for (int j = 0; j < a.length; j++) {
                if ((i & (int) Math.pow(2, j)) == 0) {
                    sum -= a[j];
                } else {
                    sum += a[j];
                }
            }
            sum = Math.abs(sum);
            if (sum < res) {
                res = sum;
            }
        }
        return res;
    }

    @Test
    public void testSolutionWithRandomArrayRepeatedly() {
        for (int i = 0; i < 1000; i++) {
            testSolutionWithRandomArray();
        }
    }
}
