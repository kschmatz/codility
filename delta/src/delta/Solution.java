package delta;

import java.util.BitSet;

public class Solution {
    private static final int RADIX = 100;

    /**
     * Solution based on partition problem algorithm.
     * 
     * @see http://people.csail.mit.edu/bdean/6.046/dp/dp_4.swf
     * @param a
     *            input array
     */
    public int solution(int[] a) {
        int n = 0;
        int c[] = new int[RADIX + 1];
        for (int v : a) {
            if (v != 0) {
                if (c[Math.abs(v)]++ == 0) {
                    n++;
                }
            }
        }
        int[] b = new int[n + 1];
        int maxSum = 0;
        for (int i = 1, j = 1; i < c.length; i++) {
            if (c[i] > 0) {
                b[j] = i;
                c[j] = c[i];
                maxSum += i * c[i];
                j++;
            }
        }
        final int half = maxSum / 2;
        BitSet[] p = new BitSet[n + 1];
        for (int i = 0; i <= n; i++) {
            p[i] = new BitSet(half + 1);
            p[i].set(0);
        }
        int[][] q = new int[n + 1][half + 1]; 
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= half; j++) {
                if (p[i - 1].get(j)) {
                    p[i].set(j);
                } else {
                    final int k = j - b[i];
                    if (k >= 0 && p[i].get(k) && q[i][k] < c[i]) {
                        p[i].set(j);
                        q[i][j] = q[i][k] + 1;
                    }
                }
            }
        }
        int j = half;
        while (j >= 0 && !p[n].get(j))
            j--;
        return maxSum - 2 * j;
    }
}