package boron;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SolutionTest {
    private Solution solver;

    @Before
    public void before() {
        solver = new Solution();
    }

    @Test
    public void testSpecification() {
        int[] a = { 1, 5, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2 };
        assertEquals(3, solver.solution(a));
    }
}
