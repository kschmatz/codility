package boron;

import java.util.ArrayList;

public class Solution {
    public int solution(int[] a) {
        ArrayList<Integer> peak = new ArrayList<Integer>();
        for (int i = 1; i < a.length - 1; i++) {
            if (a[i - 1] < a[i] && a[i] > a[i + 1]) {
                peak.add(i);
            }
        }
        int max = 0;
        for (int k = 1; k <= peak.size(); k++) {
            int flags = 1;
            int p = 0;
            int q = 0;
            while (flags < k && ++q < peak.size()) {
                if (peak.get(q) - peak.get(p) >= k) {
                    flags++;
                    p = q;
                }
            }
            if (flags > max) {
                max = flags;
            } else {
                break;
            }
        }
        return max;
    }
}
