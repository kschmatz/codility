package amazon;

import java.util.HashMap;
import java.util.TreeSet;

class Solution {
    private static int LIMIT = 100000000;
    
    public int solution(int[] a) {
        int[] unique = sortEliminateDuplicates(a);
        HashMap<Integer, Integer> pos = new HashMap<Integer, Integer>();
        for (int i = 0; i < unique.length; i++) {
            pos.put(unique[i], i);
        }
        for (int i = 0; i < a.length; i++) {
        	a[i] = pos.get(a[i]);
        }
        int count = 0;
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (Math.abs(a[i] - a[j]) <= 1) {
                    if (++count > LIMIT) {
                        return -1;
                    }
                }
            }
        }
        return count; 
    }
    
    /** Returns sorted copy of input array without duplicates */
    private static int[] sortEliminateDuplicates(int[] arr) {
        TreeSet<Integer> s = new TreeSet<Integer>();
        for (int value : arr) {
            s.add(value);
        }
        int[] res = new int[s.size()];
        int i = 0;
        for (int value : s) {
            res[i++] = value;
        }
        return res;
    }
    
    public static void main(String[] args) {
        Solution sol = new Solution();
        int[] a = { 0, 3, 3, 7, 5, 3, 11, 1};
        int n = sol.solution(a);
        System.out.println("n = " + n);
    }
}