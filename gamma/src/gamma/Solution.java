package gamma;

/**
 * In this problem we consider only strings consisting of lower-case English
 * letters (a-z).
 * <p>
 * A string is a palindrome if it reads exactly the same from left to right as
 * it does from right to left. For example, these strings are palindromes: aza,
 * abba, abacaba. These strings are not palindromes: zaza, abcd, abacada.
 * <p>
 * Given a string S of length N, a slice of S is a substring of S specified by a
 * pair of integers (p, q), such that 0 <= p < q < N. A slice (p, q) of string S
 * is palindromic if the string consisting of letters S[p], S[p+1], ..., S[q] is
 * a palindrome. For example, in a string S = abbacada:
 * <ul>
 * <li>slice (0, 3) is palindromic because abba is a palindrome,</li>
 * <li>slice (6, 7) is not palindromic because da is not a palindrome,</li>
 * <li>slice (2, 5) is not palindromic because baca is not a palindrome,</li>
 * <li>slice (1, 2) is palindromic because bb is a palindrome.</li>
 * </ul>
 */
public class Solution {
    private static final int LIMIT = 100000000;

    /**
     * Computes the number of palindromic slices of S. The function returns -1
     * if this number is greater than 100,000,000.
     * <p>
     * Implementation adapted from <a href=
     * "http://leetcode.com/2011/11/longest-palindromic-substring-part-ii.html"
     * >this algorithm</a>.
     * 
     * @param s
     *            input string
     * @return number of palindromic slices
     */
    public int solution(String s) {
        String t = transform(s);
        final int n = t.length();
        int[] p = new int[n];
        int c = 0;
        int r = 0;
        for (int i = 1; i < n - 1; i++) {
            final int iMirror = 2 * c - i;
            p[i] = (r > i) ? Math.min(r - i, p[iMirror]) : 0;
            while (t.charAt(i + 1 + p[i]) == t.charAt(i - 1 - p[i])) {
                p[i]++;
            }
            if (i + p[i] > r) {
                c = i;
                r = i + p[i];
            }
        }
        int res = 0;
        for (int v : p) {
            res += v / 2;
            if (res > LIMIT) {
                return -1;
            }
        }
        return res;
    }

    private static String transform(String s) {
        final int m = s.length();
        if (m == 0) {
            return "^$";
        } else {
            StringBuffer t = new StringBuffer(2 * m + 2);
            t.append('^');
            for (int i = 0; i < m; i++) {
                t.append('#');
                t.append(s.charAt(i));
            }
            t.append('#');
            t.append('$');
            return t.toString();
        }
    }
}
