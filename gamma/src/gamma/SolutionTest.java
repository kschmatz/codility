package gamma;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void testSolution() {
        Solution solver = new Solution();
        assertEquals(6, solver.solution("baababa"));
    }
}
