package alpha;

import java.util.HashSet;

/**
 * A non-empty zero-indexed array A consisting of N integers is given. The first
 * covering prefix of array A is the smallest integer P such that 0�P<N and such
 * that every value that occurs in array A also occurs in sequence A[0], A[1],
 * ..., A[P].
 */
public class Solution {
	public int solution(int[] a) {
		HashSet<Integer> set = new HashSet<Integer>();
		for (int value : a) {
			set.add(value);
		}
		int i = 0;
		while (i < a.length) {
			if (set.remove(a[i]) && set.isEmpty()) {
				break;
			}
			i++;
		}
		return i;
	}
}
