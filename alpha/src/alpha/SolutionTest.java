package alpha;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

	@Test
	public void testSolution() {
		int[] a = { 2, 2, 1, 0, 1};
		Solution solver = new Solution();
		assertEquals(3, solver.solution(a));
	}
}
