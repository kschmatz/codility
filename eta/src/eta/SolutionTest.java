package eta;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void testRouteConnectsDistinctTownsViolated() {
        int[] a = { 4, 4, 0, 3, 4, 1, 5, 1, 7, 6, 7, 2, 7, 1 };
        Solution solver = new Solution();
        assertEquals(-2, solver.solution(a));
    }

    @Test
    public void testEachTownIsVisitedOnceOrThriceViolated() {
        int[] a = { 3, 6, 3, 1, 5, 1, 0, 1, 3, 7, 3, 2, 3, 4 };
        Solution solver = new Solution();
        assertEquals(-2, solver.solution(a));
    }
    
    @Test
    public void testEachRouteIsVisitedTwiceViolated() {
        int[] a = { 4, 0, 5, 0, 3, 0, 1, 3, 2, 3 };
        Solution solver = new Solution();
        assertEquals(-2, solver.solution(a));
    }
    
    @Test
    public void testSolution() {
        int[] a = { 4, 0, 4, 3, 4, 1, 5, 1, 7, 6, 7, 2, 7, 1 };
        Solution solver = new Solution();
        assertEquals(3, solver.solution(a));        
    }
}
