package eta;

import java.util.HashMap;

/**
 * An island has M towns: M >= 4 and M is an even integer. The towns are
 * labeled with unique integers within the range [0..(M-1)]. The towns are
 * connected through a network of (M-1) roads. Each road is bidirectional and
 * connects exactly two distinct towns. Some towns, called cul-de-sacs, are
 * connected to just one other town. Each of the remaining towns is connected to
 * exactly three other towns. Each town can be reached from any other town.
 * <p>
 * Last year, the queen of the island went on a trip to visit all the towns
 * (eventually arriving back at the starting point). She took a route that
 * passed through each of (M-1) roads exactly twice. The sequence of towns
 * visited during this trip was logged. Each cul-de-sac town was visited exactly
 * once. Every other town had to be visited exactly three times (the final
 * arrival at the starting point was not counted as a visit). After last year's
 * trip, the queen ordered the construction of a new circular highway connecting
 * all cul-de-sac towns. The highway consists of a number of new roads,
 * connecting the cul-de-sacs in the order of the queen's last visit.
 * <p>
 * The idea was raised that, with the new roads in place, it may be possible to
 * devise a Hamiltonian route this year; i.e. one that passes through each town
 * exactly once (and arrives back at the starting point).
 */
public class Solution {
    private static int CONDITION_VIOLATED = -2;
    private static int MIN_TOWNS = 4;
    private static int MAX_TOWNS = 200000;

    /**
     * Given a zero-indexed array A consisting of N = 2*(M-1) integers
     * representing the sequence in which M towns were visited last year,
     * returns the number of different Hamiltonian routes possible this year.
     * Special cases:
     * <ul>
     * <li>The function returns -1 if the number of possible Hamiltonian routes
     * exceeds 100,000,000.</li>
     * <li>The function returns -2 if the route described by the array A
     * violates any of the following conditions:
     * <ul>
     * <li>each road connects distinct towns</li>
     * <li>each town is visited either exactly once or exactly thrice</li>
     * <li>each road is taken exactly twice</li>
     * </ul>
     * </ul>
     * 
     * @param a
     *            sequence in which the towns were visited last year
     * @return number of different Hamiltonian routes possible this year
     */
    public int solution(int[] a) {
        if (!areConditionsFulfilled(a)) {
            return CONDITION_VIOLATED;
        } else {
            // If the graph is as specified, the answer is always the same.
            return 3;
        }
    }

    /**
     * Checks the conditions for which solution() is supposed to return -2.
     * 
     * @param a
     *            sequence in which the towns were visited last year
     * @return false if any of the conditions is violated, true otherwise
     */
    private static boolean areConditionsFulfilled(int[] a) {
        final int m = a.length / 2 + 1;
        if (m < MIN_TOWNS || m > MAX_TOWNS || (m & 1) != 0) {
            return false;
        }
        int[] townsVisited = new int[m];
        RouteCounter routesVisited = new RouteCounter();
        int previousTown = -1;
        for (int town : a) {
            if (town == previousTown) {
                return false;
            }
            townsVisited[town]++;
            if (previousTown != -1) {
                routesVisited.increment(previousTown, town);
            }
            previousTown = town;
        }
        routesVisited.increment(previousTown, a[0]);
        for (int count : townsVisited) {
            if (count != 1 && count != 3) {
                return false;
            }
        }
        for (int count : routesVisited.values()) {
            if (count != 2) {
                return false;
            }
        }
        return true;
    }
}

class RouteCounter {
    private final HashMap<UndirectedEdge, Integer> counter;
    
    public RouteCounter() {
        counter = new HashMap<UndirectedEdge, Integer>();
    }
    
    public void increment(int from, int to) {
        UndirectedEdge e = new UndirectedEdge(from, to);
        Integer value = counter.get(e);
        int newValue = (value == null) ? 1 : value + 1;
        counter.put(e, newValue);
    }
    
    public Iterable<Integer> values() {
        return counter.values();
    }
}

class UndirectedEdge {
    private final int node;
    private final int other;

    public UndirectedEdge(int node, int other) {
        if (node < other) {
            this.node = node;
            this.other = other;
        } else {
            this.node = other;
            this.other = node;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof UndirectedEdge)) {
            return false;
        }
        UndirectedEdge edge = (UndirectedEdge) o;
        return node == edge.node && other == edge.other;
    }

    @Override
    public int hashCode() {
        return 31 * node + other;
    }
}