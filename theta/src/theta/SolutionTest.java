package theta;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void testSolution() {
        int[] d = { 10, 9, 8 };
        int[] p = { 2, 1, 3 };
        Solution solver = new Solution();
        assertEquals(41, solver.solution(d, p, 15));
    }

}
