package theta;

/**
 * <p>
 * There are N+1 towns numbered from 0 to N lying along a highway. The distances
 * between towns, and gas prices in each town, are given. A truck has to deliver
 * cargo from town 0 to town N. What is the cheapest way to buy enough gas for
 * this trip?
 * 
 * <p>
 * Two non-empty zero-indexed arrays D (distances) and P (prices), each
 * consisting of N positive integers, and a positive integer T (tank capacity)
 * are given. Consider any zero-indexed sequence R (refill strategy) consisting
 * of N non-negative integers (amount of fuel bought in each town).
 * 
 * <p>
 * Sequence L (amount of fuel when leaving town), consisting of N integers, is
 * defined as follows:
 * 
 * <p>
 * L[K] = (R[0] + ... + R[K]) - (D[0] + ... + D[K-1]) for 0 � K � N-1
 * 
 * <p>
 * Sequence A (amount of fuel when arriving at town), consisting of N integers,
 * is defined as follows: A[K] = L[K-1] - D[K-1] for 1 � K � N
 * 
 * <p>
 * The following conditions (meaning that the truck must not run out of fuel and
 * cannot fill up with more fuel than its tank capacity) must hold:
 * <ul>
 * <li>0 � L[K] � T for 0 � K � N-1</li>
 * <li>0 � A[K] � T for 1 � K � N</li>
 * </ul>
 * 
 * <p>
 * Number C (total cost of refill strategy) is defined as: C = R[0] * P[0] + ...
 * + R[N-1] * P[N-1]
 * 
 * @param d
 *            distances between towns
 * @param p
 *            gas prices in each town
 * @param t
 *            tank capacity
 * @return cost of cheapest refill strategy, -1 if no valid refill strategy
 *         exists, -2 if result exceeds 10^9
 */
public class Solution {
    private static int LIMIT = 1000000000;

    public int solution(int d[], int p[], int t) {
        for (int v : d) {
            if (v > t) {
                return -1;
            }
        }
        final int n = d.length;
        int[] r = new int[n];
        long cost = 0;
        int a = 0;
        int i = 0;
        while (i < n) {
            long dist = d[i];
            int j = i + 1;
            int minPrice = Integer.MAX_VALUE;
            int minPriceIndex = -1;
            int minPriceDist = 0;
            boolean searchingLowerPrice = true;
            while (searchingLowerPrice && dist <= t) {
                if (j == n || p[j] <= p[i]) {
                    searchingLowerPrice = false;
                } else {
                    if (p[j] <= minPrice) {
                        minPrice = p[j];
                        minPriceIndex = j;
                        minPriceDist = (int) dist;
                    }
                    dist += d[j++];
                }
            }
            if (searchingLowerPrice) {
                r[i] = t - a;
                cost += (long) r[i] * p[i];
                a = t - minPriceDist;
                i = minPriceIndex;
            } else {
                r[i] = (int) dist - a;
                cost += (long) r[i] * p[i];
                a = 0;
                i = j;
            }
            if (cost > LIMIT) {
                return -2;
            }
        }
        return (int) cost;
    }
}
