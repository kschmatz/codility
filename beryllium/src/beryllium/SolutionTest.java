package beryllium;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SolutionTest {
    private Solution solver;
    
    @Before
    public void before() {
        solver = new Solution();
    }
    
    @Test
    public void testSpecification() {
        int[] a = { 3, 5, 7, 3, 3, 5 };
        assertEquals(14, solver.solution(a));
    }
    
    @Test
    public void testOneElement() {
        int[] a = { 3 };
        assertEquals(1, solver.solution(a));
    }
    
    @Test
    public void testTwoDistinctElements() {
        int[] a = { 3, 5 };
        assertEquals(1, solver.solution(a));
    }
    
    @Test
    public void testTwoEqualElements() {
        int[] a = { 3, 3 };
        assertEquals(4, solver.solution(a));
    }

    @Test
    public void testThreeEqualElements() {
        int[] a = { 3, 3, 3 };
        assertEquals(9, solver.solution(a));
    }
    
    @Test
    public void testHugeArray() {
        int[] a = new int[40000];
        for (int i = 0; i < a.length; i++) {
            a[i] = i;
        }
        assertEquals(1, solver.solution(a));
    }
}
