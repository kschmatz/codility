package beryllium;

import java.util.Set;
import java.util.TreeSet;

/**
 * A non-empty zero-indexed array A consisting of N integers is given. A
 * prefix_suffix_set is a pair of indices (P, S) such that 0 <= P, S < N and such
 * that: every value that occurs in the sequence A[0], A[1], ..., A[P] also
 * occurs in the sequence A[S], A[S + 1], ..., A[N - 1], every value that occurs
 * in the sequence A[S], A[S + 1], ..., A[N - 1] also occurs in the sequence
 * A[0], A[1], ..., A[P]. The goal is to calculate the number of
 * prefix_suffix_sets in the array. For example, consider array A such that:
 * 
 * A[0] = 3 A[1] = 5 A[2] = 7 A[3] = 3 A[4] = 3 A[5] = 5
 * 
 * There are exactly fourteen prefix_suffix_sets: (1, 4), (1, 3), (2, 2), (2,
 * 1), (2, 0), (3, 2), (3, 1), (3, 0), (4, 2), (4, 1), (4, 0), (5, 2), (5, 1),
 * (5, 0).
 * 
 * @author Klaus-Dieter Schmatz
 */
public class Solution {
    private int LIMIT = 1000000000;

    public int solution(int[] a) {
        final int n = a.length;
        Set<Integer> pset = new TreeSet<Integer>();
        Set<Integer> sset = new TreeSet<Integer>();
        int matches = 0;
        int res = 0;
        for (int p = 0, s = n - 1; p < n && s >= 0;) {
            final int p0 = p;
            pset.add(a[p0]);
            while (++p < n && pset.contains(a[p]))
                ;
            final int s0 = s;
            sset.add(a[s0]);
            while (--s >= 0 && sset.contains(a[s]))
                ;
            if (a[p0] == a[s0]) {
                matches++;
            } else {
                if (pset.contains(a[s0])) {
                    matches++;
                }
                if (sset.contains(a[p0])) {
                    matches++;
                }
            }
            if (matches == pset.size()) {
                res += (p - p0) * (s0 - s);
                if (res > LIMIT) {
                    return LIMIT;
                }
            }
        }
        return res;
    }
}