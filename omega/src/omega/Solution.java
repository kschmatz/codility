package omega;

/**
 * There is an old dry well. Its sides are made of concrete rings. Each such
 * ring is one meter high, but the rings can have different (internal)
 * diameters. Nevertheless, all the rings are centered on one another. The well
 * is N meters deep; that is, there are N concrete rings inside it. You are
 * about to drop M concrete disks into the well. Each disk is one meter thick,
 * and different disks can have different diameters. Once each disk is dropped,
 * it falls down until:
 * <ul>
 * <li>it hits the bottom of the well;</li>
 * <li>it hits a ring whose internal diameter is smaller then the disk's
 * diameter; or</li>
 * <li>* it hits a previously dropped disk.</li>
 * </ul>
 * (Note that if the internal diameter of a ring and the diameter of a disk are
 * equal, then the disk can fall through the ring.) The disks you are about to
 * drop are ready and you know their diameters, as well as the diameters of all
 * the rings in the well. The question arises: how many of the disks will fit
 * into the well?
 */
public class Solution {
    /**
     * Computes the number of rings that will fit in the well.
     * 
     * @param a
     *            internal diameters of the N rings (in top-down order)
     * @param b
     *            diameters of the M disks (in the order they are to be dropped)
     * @return number of disks that will fit into the well
     */
    public int solution(int[] a, int[] b) {
        final int n = a.length;
        final int m = b.length;
        int[] amin = new int[n];
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            amin[i] = min = Math.min(min, a[i]);
        }
        int j = 0;
        for (int i = n - 1; i >= 0; i--) {
            if (b[j] <= amin[i]) {
                if (++j == m) {
                    break;
                }
            }
        }
        return j;
    }
}
