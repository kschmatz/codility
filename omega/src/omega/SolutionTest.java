package omega;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void testSolution() {
        int[] a = { 5, 6, 4, 3, 6, 2, 3 };
        int[] b = { 2, 3, 5, 2, 4 };
        Solution solver = new Solution();
        assertEquals(4, solver.solution(a, b));
    }
}
