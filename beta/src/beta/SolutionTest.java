package beta;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void testSolution() {
        int[] a = { 1, 5, 2, 1, 4, 0 };
        Solution solver = new Solution();
        assertEquals(11, solver.solution(a));
    }
}
