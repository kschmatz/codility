package beta;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class IntervalTest {

    @Test
    public void testBinarySearch() {
        Interval[] a = new Interval[6];
        a[0] = new Interval(-4, 6);
        a[1] = new Interval(-1, 1);
        a[2] = new Interval(0, 4);
        a[3] = new Interval(0, 8);
        a[4] = new Interval(2, 5);
        a[5] = new Interval(5, 5);
        assertEquals(6, Interval.binarySearch(a, 6));
        assertEquals(4, Interval.binarySearch(a, 1));
        assertEquals(5, Interval.binarySearch(a, 4));
        assertEquals(6, Interval.binarySearch(a, 8));
        assertEquals(6, Interval.binarySearch(a, 5));
    }
}
