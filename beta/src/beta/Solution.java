package beta;

import java.util.Arrays;

/**
 * Given an array A of N integers, we draw N discs in a 2D plane such that the
 * I-th disc is centered on (0,I) and has a radius of A[I]. We say that the J-th
 * disc and K-th disc intersect if J � K and J-th and K-th discs have at least
 * one common point. The class computes the number of pairs of intersecting
 * discs.
 */
public class Solution {
    private static final int LIMIT = 10000000;

    public int solution(int[] a) {
        Interval[] intervals = new Interval[a.length];
        for (int i = 0; i < a.length; i++) {
            intervals[i] = new Interval(i - (long) a[i], i + (long) a[i]);
        }
        Arrays.sort(intervals);
        int count = 0;
        for (int i = 0; i < intervals.length; i++) {
            final int j = Interval.binarySearch(intervals, intervals[i].right);
            count += j - i - 1;
            if (count > LIMIT) {
                return -1;
            }
        }
        return count;
    }

}

/**
 * Represents an interval with a left and right end.
 */
class Interval implements Comparable<Interval> {
    public final long left, right;

    public Interval(long left, long right) {
        this.left = left;
        this.right = right;
    }

    public int compareTo(Interval o) {
        if (left < o.left) {
            return -1;
        } else if (left > o.left) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Returns an insertion point which comes after (to the right of) any
     * existing entries of key in intervals.
     */
    static int binarySearch(Interval[] intervals, long key) {
        int lo = 0;
        int hi = intervals.length;
        while (lo < hi) {
            final int mid = lo + (hi - lo) / 2;
            if (key < intervals[mid].left) {
                hi = mid;
            } else {
                lo = mid + 1;
            }
        }
        return lo;
    }
}