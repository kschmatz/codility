package zeta;

/**
 * A board consisting of ball switches arranged into N rows and M columns is
 * given. A ball switch is a device that can change the direction of a rolling
 * ball. It has the following properties:
 * <ul>
 * <li>it is square-shaped;</li>
 * <li>it can be in one of three modes: -1, 0 or +1;</li>
 * <li>a ball can enter it through the top or the left edge;</li>
 * <li>a ball can leave it through the bottom or the right edge;</li>
 * <li>if the mode is -1, a ball entering the device will leave it through the
 * bottom edge;</li>
 * <li>if the mode is +1, a ball entering the device will leave it through the
 * right edge;</li>
 * <li>if the mode is 0, a ball entering the device will not change direction;</li>
 * <li>after a ball leaves the device, its mode is immediately negated.</li>
 * </ul>
 * K balls are rolled through the board one by one. Each ball enters the board
 * through the top edge of the top-left ball switch. Then it rolls through the
 * board and possibly changes its direction depending on the modes of the
 * switches through which it passes. Eventually it leaves the board either:
 * through the bottom edge of one of the switches in the bottom row; or through
 * the right edge of one of the switches in the rightmost column.
 */
public class Solution {
    /**
     * Computes the number of balls that will leave the board through the bottom
     * edge of the right bottom-right switch.
     * 
     * @param a
     *            The matrix a describes the board as follows:
     *            <ul>
     *            <li>each element of the matrix has value -1, 0 or +1 and
     *            describes the initial mode of the corresponding ball switch;</li>
     *            <li>element A[0][0] corresponds to the top-left ball switch;</li>
     *            <li>element A[N-1][M-1] corresponds to the bottom-right ball
     *            switch.</li>
     *            </ul>
     * @param k
     *            number of balls
     * @return number of balls that will leave the board through the bottom edge
     *         of the bottom-right switch
     */
    public int solution(int[][] a, int k) {
        final int n = a.length;
        final int m = a[0].length;
        int[] bottom = new int[m];
        int[] right = new int[m];
        bottom[0] = k;
        for (int row = 0; row < n; row++) {
            int ballsHeadingRight = 0;
            for (int col = 0; col < m; col++) {
                if (a[row][col] == 0) {
                    right[col] = ballsHeadingRight;
                } else {
                    int incomingBalls = bottom[col] + ballsHeadingRight;
                    if (a[row][col] == -1) {
                        right[col] = incomingBalls / 2;
                        bottom[col] = incomingBalls - right[col];
                    } else {
                        bottom[col] = incomingBalls / 2;
                        right[col] = incomingBalls - bottom[col];
                    }
                    ballsHeadingRight = right[col];
                }
            }
        }
        return bottom[m - 1];
    }
}