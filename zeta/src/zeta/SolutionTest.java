package zeta;

import static org.junit.Assert.*;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void test() {
        int[][] a = { { -1, 0, -1 }, { 1, 0, 0 } };
        Solution solver = new Solution();
        assertEquals(1, solver.solution(a, 4));
    }
}
