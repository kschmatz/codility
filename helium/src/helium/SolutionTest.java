package helium;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {
    private Solution solver = new Solution();

    @Test
    public void testSolutionReturns1() {
        assertEquals(1, solver.solution("barbararhubarb"));
    }

    @Test
    public void testSolutionReturns2() {
        assertEquals(2, solver.solution("ababab"));
    }
    
    @Test
    public void testSolutionReturns0() {
        assertEquals(0, solver.solution("baaab"));
    }

}
