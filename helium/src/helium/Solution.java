package helium;

/**
 * A prefix of a string S is any leading contiguous part of S. A suffix of the
 * string S is any trailing contiguous part of S. For example, "c" and "cod" are
 * prefixes, and "ty" and "ity" are suffixes of the string "codility". For
 * simplicity, we require prefixes and suffixes to be non-empty and shorter than
 * the whole string S. A border of a string S is any string that is both a
 * prefix and a suffix. For example, "cut" is a border of a string "cutletcut",
 * and a string "barbararhubarb" has two borders: "b" and "barb". We are looking
 * for such borders of S that have at least three non-overlapping occurrences;
 * that is, for some string that occurs as a prefix, as a suffix and elsewhere
 * in between. For example, for S = "barbararhubarb", the only such string is
 * "b". In this problem we consider only strings that consist of lower-case
 * English letters (a-z).
 * 
 * @author Klaus-Dieter Schmatz
 */
public class Solution {
    /**
     * Computes the length of the longest border of a string that has at least
     * three non-overlapping occurrences in the given string.
     * 
     * @param s
     *            input string
     * @return length of longest border of s with at least three non-overlapping
     *         occurrences in s; if not such border exists, the return value is
     *         0
     */
    public int solution(String s) {
        final int n = s.length();
        final byte[] t = s.getBytes();
        for (int i = n / 3; i > 0; i--) {
            if (contains(t, 0, i, n - i, n))
                if (contains(t, 0, i, i, n - i)) {
                    return i;
            }
        }
        return 0;
    }

    private static boolean contains(byte[] s, int patternBegin, int patternEnd,
            int searchBegin, int searchEnd) {
        final int patternLength = patternEnd - patternBegin;
        final int searchLoopEnd = searchEnd - patternLength;
        for (int i = searchBegin; i <= searchLoopEnd; i++) {
            for (int j = 0; s[patternBegin + j] == s[i + j]; ) {
                if (++j == patternLength) {
                    return true;
                }
            }
        }
        return false;
    }
}
