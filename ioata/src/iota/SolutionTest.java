package iota;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void test() {
        int[] a = { 1, 10, 6, 5, 10, 7, 5, 2 };
        Solution solver = new Solution();
        assertEquals(4, solver.solution(a));
    }

}
