package iota;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class Solution {
    public int solution(int[] a) {
        // Create graph representation using adjacency lists (sets).
        Map<Integer, Set<Integer>> adj = new HashMap<Integer, Set<Integer>>();
        for (int i = 1; i < a.length; i++) {
            addNeighbor(adj, a[i - 1], a[i]);
            addNeighbor(adj, a[i], a[i - 1]);
        }
        // Run breadth-first search in graph.
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.offer(a[0]);
        Map<Integer, Integer> len = new HashMap<Integer, Integer>();
        len.put(a[0], 1);
        while (!queue.isEmpty()) {
            int t = queue.poll();
            int lenT = len.get(t);
            if (t == a[a.length - 1]) {
                return lenT;
            }
            for (int neighbor : adj.get(t)) {
                if (!len.containsKey(neighbor)) {
                    len.put(neighbor, lenT + 1);
                    queue.offer(neighbor);
                }
            }
        }
        return -1;
    }

    private static void addNeighbor(Map<Integer, Set<Integer>> adj, int p, int q) {
        Set<Integer> neighborsOfP = adj.get(p);
        if (neighborsOfP == null) {
            neighborsOfP = new HashSet<Integer>();
            adj.put(p, neighborsOfP);
        }
        neighborsOfP.add(q);
    }
}