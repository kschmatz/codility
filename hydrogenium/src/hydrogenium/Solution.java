package hydrogenium;

public class Solution {
    /**
     * Straightforward O(N^2) implementation of Dijkstra's single source
     * shortest path algorithm
     */
    public int solution(int[] a, int[] b, int[] c, int[] d) {
        final int m = a.length;
        final int n = d.length;
        int[] dist = new int[n];
        int[][] adj = new int[n][n];
        for (int i = 0; i < n; i++) {
            dist[i] = Integer.MAX_VALUE;
            for (int j = 0; j < n; j++) {
                adj[i][j] = Integer.MAX_VALUE;
            }
        }
        for (int i = 0; i < m; i++) {
            adj[a[i]][b[i]] = Math.min(adj[a[i]][b[i]], c[i]);
            adj[b[i]][a[i]] = Math.min(adj[b[i]][a[i]], c[i]);
        }
        dist[0] = 0;
        boolean[] visited = new boolean[n];
        while (true) {
            int minDist = Integer.MAX_VALUE;
            int minDistIndex = -1;
            for (int i = 0; i < n; i++) {
                if (!visited[i] && dist[i] < minDist) {
                    minDist = dist[i];
                    minDistIndex = i;
                }
            }
            if (minDist == Integer.MAX_VALUE) {
                break;
            }
            for (int i = 0; i < n; i++) {
                if (adj[minDistIndex][i] != Integer.MAX_VALUE) {
                    int option = dist[minDistIndex] + adj[minDistIndex][i];
                    if (option < dist[i]) {
                        dist[i] = option;
                    }
                }
            }
            visited[minDistIndex] = true;
        }
        int minDist = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            if (dist[i] <= d[i]) {
                if (dist[i] < minDist) {
                    minDist = dist[i];
                }
            }
        }
        return (minDist == Integer.MAX_VALUE) ? -1 : minDist;
    }
}
