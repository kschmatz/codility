package hydrogenium;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void testSolution() {
        int[] a = { 0, 1, 3, 1, 2, 2 };
        int[] b = { 1, 2, 2, 3, 0, 1 };
        int[] c = { 2, 3, 4, 5, 7, 5 };
        int[] d = { -1, 1, 3, 8 };
        Solution solver = new Solution();
        assertEquals(7, solver.solution(a, b, c, d));
    }

    @Test
    public void testSolutionNoSolutionExists() {
        int[] a = { 0 };
        int[] b = { 1 };
        int[] c = { 10 };
        int[] d = { -1, 6, 8 };
        Solution solver = new Solution();
        assertEquals(-1, solver.solution(a, b, c, d));
    }

    @Test
    public void testSolutionNotConnectedGraph() {
        int[] a = { 6, 6, 3, 8, 8, 6, 7, 5, 1, 4, 3, 2, 7, 7 };
        int[] b = { 3, 7, 5, 8, 0, 6, 3, 4, 1, 7, 1, 5, 3, 2 };
        int[] c = { 8, 1, 9, 12, 11, 1, 8, 12, 3, 6, 12, 7, 4, 2 };
        int[] d = { -1, 1000000000, 1000000000, 999999999, 999999999,
                999999999, 1000000000, 1000000000, 1000000000 };
        Solution solver = new Solution();
        assertEquals(11, solver.solution(a, b, c, d));
    }

    @Test
    public void testSolutionParallelEdges() {
        int[] a = { 0, 2, 3, 1, 4, 1, 0, 2, 1 };
        int[] b = { 0, 3, 2, 4, 4, 3, 3, 2, 2 };
        int[] c = { 10, 2, 4, 7, 1, 9, 1, 3, 9 };
        int[] d = { -1, 1000000000, 899, 0, 999999999 };
        Solution solver = new Solution();
        assertEquals(3, solver.solution(a, b, c, d));
    }
}
