package equi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void testSolution() {
        int[] a = { -7, 1, 5, 2, -4, 3, 0 };
        Solution solver = new Solution();
        int res = solver.solution(a);
        assertEquals(3, res);
    }

}
