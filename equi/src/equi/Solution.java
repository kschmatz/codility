package equi;

public class Solution {
    public int solution(int[] a) {
        long total = 0;
        for (int i = 0; i < a.length; i++) {
            total += a[i];
        }
        long leftSum = 0;
        for (int i = 0; i < a.length; i++) {
            if (leftSum == total - leftSum - a[i]) {
                return i;
            }
            leftSum += a[i];
        }
        return -1;
    }
}
