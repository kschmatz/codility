package lithium;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SolutionTest {
    private Solution solver;

    @Before
    public void before() {
        solver = new Solution();
    }
    
    @Test
    public void test1() {
        int[][] a = { { 1, 2 }, { 2, 4 }, { 4, 3 }, { 2, 3 }, { 1, 3 } };
        assertEquals(4, solver.solution(a, 4));
    }

    @Test
    public void test2() {
        int[][] a =
                { { 7, 16, 20, 24 }, { 5, 14, 18, 22 }, { 6, 7, 10, 15 },
                        { 6, 7, 10, 15 }, { 3, 7, 11, 18 }, { 4, 8, 12, 19 } };
        assertEquals(7, solver.solution(a, 24));
    }

    @Test
    public void test3() {
        int[][] a = { { 1, 2 }, { 3, 1 }, { 1, 3 } };
        assertEquals(3, solver.solution(a, 3));
    }

    @Test
    public void test4() {
        int[][] a = { { 1, 2 }, { 3, 1 } };
        assertEquals(0, solver.solution(a, 4));
    }
    
    @Test
    public void test5() {
        int[][] a = { { 4, 5, 6 }, { 9999, 1, 10000 }, { 3, 2, 1 }, { 7, 9, 10 } };
        assertEquals(3, solver.solution(a, 10000));
    }
    
    @Test
    public void test6() {
        int[][] a =
                { { 3, 14, 17, 21 }, { 5, 6, 12, 15 }, { 6, 9, 13, 19 },
                        { 1, 7, 18, 21 }, { 8, 11, 15, 21 }, { 3, 7, 13, 24 } };
        assertEquals(10, solver.solution(a, 24));
    }

    @Test
    public void testEqualElements() {
        int[][] a = { { 1, 2, 3, 4 }, { 1, 2, 3, 4 }, { 1, 2, 3, 4 }, { 1, 2, 3, 4 } };
        assertEquals(6, solver.solution(a, 11));
    }
    
    @Test
    public void testOneHandOneClock() {
        int[][] a = { { 1 } };
        assertEquals(0, solver.solution(a, 1));
    }
    
    @Test
    public void testOneHandTwoClocks() {
        int[][] a = { { 1 }, { 1 } };
        assertEquals(1, solver.solution(a, 1));
    }

    @Test
    public void testOneClock() {
        int[][] a = { { 1, 2 } };
        assertEquals(0, solver.solution(a, 12));
    }
    
    @Test
    public void testLargeElement() {
        int[][] a = { { 1000000000, 1 }, { 1, 2 } };
        assertEquals(1, solver.solution(a, 1000000000));
    }
    
    @Test
    public void testHugeArray() {
        final int n = 500;
        final int m = 500;
        int[][] a = new int[n][m];
        int k = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = k++;
            }
        }
        assertEquals(n * (n -1) / 2 , solver.solution(a, 1000000000));
    }
}
