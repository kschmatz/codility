package lithium;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {
    public int solution(int[][] a, int p) {
        Map<Clock, Counter> map = new HashMap<Clock, Counter>();
        for (int i = 0; i < a.length; i++) {
            Clock clock = new Clock(a[i], p);
            Counter counter = map.get(clock);
            if (counter == null) {
                counter = new Counter();
                map.put(clock, counter);
            }
            counter.increment();
        }
        int res = 0;
        for (Counter counter : map.values()) {
            res += counter.intValue() * (counter.intValue() - 1) / 2;
        }
        return res;
    }
}

class Counter {
    private int value;
    
    public void increment() {
        value++;
    } 
    
    public int intValue() {
        return value;
    }  
}

class Clock {
    private final int[] distToPred;
    private final int hash;
    private int[] kmp;
    
    public Clock(int[] a, int p) {
        final int m = a.length;
        distToPred = Arrays.copyOf(a, m);
        Arrays.sort(distToPred);
        int h = 0;
        int pred = distToPred[m - 1] - p;
        for (int i = 0; i < m; i++) {
            int curr = distToPred[i];
            distToPred[i] = curr - pred;
            h += distToPred[i] * distToPred[i];
            pred = curr;
        }
        hash = h;
    }
    
    @Override
    public int hashCode() {
        return hash;
    }
    
    @Override
    public boolean equals(Object other) {
        if (other instanceof Clock) {
            prepareKMP();
            return matchKMP(((Clock) other).distToPred);         

//   Brute force solution (scores 95%):
//            
//            final Clock clock = (Clock) other;
//            for (int i = 0; i < m; i++) {
//                boolean equal = false;
//                for (int j = 0; j < m; j++) {
//                    equal = distToPred[j] == clock.distToPred[(j + i) % m];
//                    if (!equal) {
//                        break;
//                    }
//                }
//                if (equal) {
//                    return true;
//                }
//            }
        }
        return false;
    }
    
    private void prepareKMP() {
        if (kmp == null) {
            final int m = distToPred.length;
            kmp = new int[m + 1];
            int i = 0;
            int j = -1;
            kmp[i] = j;
            while (i < m) {
                while (j >= 0 && distToPred[j] != distToPred[i]) {
                    j = kmp[j];
                }
                kmp[++i] = ++j;
            }
        }
    }

    private boolean matchKMP(int[] t) {
        final int m = t.length;
        int j = 0;
        for (int i = 0; i < m * 2; i++) {
            while (j >= 0 && t[i % m] != distToPred[j]) {
                j = kmp[j];
            }
            if (++j == m) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append('[');
        for (int val : distToPred) {
            str.append(' ');
            str.append(val);
        }
        str.append(" ]");
        return str.toString();
    }    
}
